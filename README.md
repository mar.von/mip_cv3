# MIP_cv3

## Popis řešení
V hlavním vlákně se registruje zmáčknutí displeje a spouští se vlákna. Uvnitř vláken se pomocí metod acquire() a release() ovládá semafor.


## Zadání
Napiště kód, jenž vytvoří nejméně 4 vlákna. Každé z vláken chce pracovat s displejem a mít jej po určitou dobu vyhrazen jen pro sebe. K synchronizaci na displeji vlákna používají semafor (z mbed.com).
Vlákna se aktivují dotykem na displej, kdykoliv se dotknete, aktivuje se další vlákno v pořadí.

Je zcela na vás, co vlákna s displejem budou dělat, jen musí být patrné, které z vláken zrovna pracuje. Fantazii se meze nekladou, vtipná řešení jsou vítána, bude nás to všechny víc bavit. 


## Co je cílem
Naučit se správně synchronizovat vlákna v malém OS, nebo i ve velkém programu. Vývoj pro minipočítač totiž typicky zahrnuje práci s různými nezávislými vlákny a my nechceme dopadnout jako stále se resetující sonda na Marsu ;)