#include "mbed.h"
#include "rtos.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include <list>

#define WATCHDOG_TIMEOUT 7000

TS_StateTypeDef TS_State;
Semaphore one_slot(1, 1);
int numberOfThreads = 4;
Thread threads[4];
volatile int repeats[4] = {1, 1, 1, 1};
volatile uint32_t colors [] = {LCD_COLOR_YELLOW, LCD_COLOR_LIGHTGREEN, LCD_COLOR_LIGHTBLUE, LCD_COLOR_CYAN};
int clickNumber = 0;

bool touch()
{
    BSP_TS_GetState(&TS_State);
    if(TS_State.touchDetected) 
    {
        return true;
    }
    return false;   
}

void threadTakesCharge(int* threadNumber)
{
    while(1)
    {
        if(repeats[(uint32_t)threadNumber] > 0)
        {
            one_slot.acquire();
            uint8_t text[50];
            BSP_LCD_SetTextColor(LCD_COLOR_BLACK);   
            BSP_LCD_Clear(colors[(uint32_t)threadNumber]);
            BSP_LCD_SetBackColor(colors[(uint32_t)threadNumber]);
            sprintf((char*)text, "Thread %d: NOOOOO!!!!", threadNumber);
            BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)&text, CENTER_MODE);
            sprintf((char*)text, "I AM IN CHARGE!!!");
            BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)&text, CENTER_MODE);
            ThisThread::sleep_for(3s);
            
            BSP_LCD_SetTextColor(LCD_COLOR_BLACK);   
            BSP_LCD_Clear(LCD_COLOR_WHITE);
            BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
            sprintf((char*)text, "No one is in charge again");
            BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)&text, CENTER_MODE);     
            repeats[(uint32_t)threadNumber]--;
            one_slot.release(); 
        }       
    }   
}

int main() 
{   
    uint8_t text[30]; 
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
    
    uint8_t status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
    
    if (status != TS_OK) {
        BSP_LCD_Clear(LCD_COLOR_RED);
        BSP_LCD_SetBackColor(LCD_COLOR_RED);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)"TOUCHSCREEN INIT FAIL", CENTER_MODE);
    } else {
        BSP_LCD_Clear(LCD_COLOR_GREEN);
        BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)"TOUCHSCREEN INIT OK", CENTER_MODE);
    }
    HAL_Delay(1000);
    
    
    BSP_LCD_SetFont(&Font24);
    BSP_LCD_Clear(LCD_COLOR_BLACK);
    BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    
    sprintf((char*)text, "No one is in charge...");
    BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)&text, CENTER_MODE);
    
    while(1)
    {
        bool touched = touch();
        if(touched && clickNumber < numberOfThreads)
        {
            threads[clickNumber].start(callback(threadTakesCharge, (int*)clickNumber));
            HAL_Delay(300);
            clickNumber++;
        }  
        else if(touched && clickNumber >= numberOfThreads)
        {
            repeats[clickNumber % numberOfThreads]++;
            HAL_Delay(300);
            clickNumber++;
        }
    }
}
